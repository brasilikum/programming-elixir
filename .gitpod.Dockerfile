FROM gitpod/workspace-postgres
LABEL maintainer="Ganesh Gunasegaran <me@itsgg.com>"

USER root
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y jq wget curl unzip git

ENV LANG C.UTF-8

USER gitpod
ENV PATH /home/gitpod/.asdf/bin:/home/gitpod/.asdf/shims:$PATH

RUN /bin/bash -c "git clone https://github.com/asdf-vm/asdf.git /home/gitpod/.asdf"
RUN /bin/bash -c "asdf plugin-add erlang"
RUN /bin/bash -c "asdf install erlang 22.2.4"
RUN /bin/bash -c "asdf global erlang 22.2.4"
RUN /bin/bash -c "asdf plugin-add elixir"
RUN /bin/bash -c "asdf install elixir 1.10.0"
RUN /bin/bash -c "asdf global elixir 1.10.0"
RUN /bin/bash -c "rm -rf /tmp/*"
RUN /bin/bash -c "mix local.hex --force && mix local.rebar --force"
RUN /bin/bash -c "mix archive.install hex phx_new 1.5.4 --force"
#RUN /bin/bash -c "mix deps.get && mix ecto.setup"
#RUN /bin/bash -c "cd assets && npm install"

USER root
WORKDIR /home/gitpod/
COPY . .
